from django import forms

from users.models import LearningUser


class SignupForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = LearningUser
        fields = ('username', 'email')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords are not equal")
        return password1

    def clean_username(self):
        username = self.cleaned_data['username']
        if self.cleaned_data['username'].lower() == 'oleg':
            raise forms.ValidationError('Oleg is not allowed here!')
        return username

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class EmailConfirmForm(forms.Form):
    token = forms.CharField(max_length=32)
