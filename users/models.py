import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class LearningUser(AbstractUser):
    email_confirmation = models.BooleanField(default=False)
    confirmation_token = models.CharField(max_length=80)

    def save(self, *args, **kwargs):
        self.confirmation_token = str(uuid.uuid4()).replace('-', '')
        super(LearningUser, self).save(*args, **kwargs)
