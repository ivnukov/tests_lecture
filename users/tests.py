from django.test import TestCase, Client

# Create your tests here.
from users.models import LearningUser


class UserTestCase(TestCase):
    def setUp(self) -> None:
        self.client = Client()

    def test_user_token(self):
        user = LearningUser.objects.create(username="test", email='test@example.com')
        self.assertTrue(len(user.confirmation_token) == 32)

    def test_signup_with_different_password(self):
        username = 'test1'
        password1 = '123qweQWE!@#'
        password2 = '123qweQWE123'

        response = self.client.post('/signup/', dict(username=username,
                                                     password1=password1,
                                                     password2=password2))
        self.assertEqual(response.status_code, 200)
        self.assertIn('Passwords are not equal', response.content.decode('utf-8'))

    def test_signup_with_exits_user(self):
        username = 'test1'
        password1 = '123qweQWE!@#'
        password2 = '123qweQWE!@#'

        exist_user = LearningUser.objects.create(username='test1')

        response = self.client.post('/signup/', dict(username=username,
                                                     password1=password1,
                                                     password2=password2))
        self.assertEqual(response.status_code, 200)
        self.assertIn('A user with that username already exists.',
                      response.content.decode('utf-8'))

    def test_signup_works_correct(self):
        username = 'test1'
        password1 = '123qweQWE!@#'
        password2 = '123qweQWE!@#'

        response = self.client.post('/signup/', dict(username=username,
                                                     password1=password1,
                                                     password2=password2))

        self.assertEquals(302, response.status_code)
        self.assertEquals(response.url, '/')
        self.assertEquals(LearningUser.objects.count(), 1)

    def test_signup_with_oleg_username(self):
        username = 'oleg'
        password1 = '123qweQWE!@#'
        password2 = '123qweQWE!@#'

        response = self.client.post('/signup/', dict(username=username,
                                                     password1=password1,
                                                     password2=password2))
        self.assertEquals(response.status_code, 200)
        self.assertEquals(LearningUser.objects.count(), 0)

        self.assertIn('Oleg is not allowed here!', response.content.decode('utf-8'))

