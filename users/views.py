from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
# Create your views here.
from django.views.generic import CreateView, TemplateView, FormView

from users.forms import SignupForm, EmailConfirmForm


class HomePageView(LoginRequiredMixin, TemplateView):
    login_url = 'login/'
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            confirm_form = EmailConfirmForm()
            context.update(dict(confirm_form=confirm_form))
        return context


class SignupView(CreateView):
    form_class = SignupForm
    success_url = '/'
    template_name = 'signup.html'


class EmailConfirmView(FormView, LoginRequiredMixin):
    form_class = EmailConfirmForm
    template_name = 'confirm_email.html'
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            token = form.cleaned_data['token']
            if token == self.request.user.confirmation_token:
                self.request.user.email_confirmation = True
                self.request.user.save()
            else:
                messages.add_message(self.request, messages.ERROR, 'Invalid Token')
        return HttpResponseRedirect('/')
